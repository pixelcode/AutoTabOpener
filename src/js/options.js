window.browser = window.browser || window.chrome;

var atoURLs = new Object(),
  pinned = [],
  regular = [];

// --------------------
// Load all stored URLs
// --------------------
function fillTabs(){
  browser.storage.local.get('atoURLsJSON').then(function(tabs) {
    return gotTabs(tabs);
  }).catch((error) => {
    console.error(error);
  });
}

function gotTabs(tabs){
  var urls = `${tabs.atoURLsJSON}`;
  $('#options-URLsTextArea').val('');

  if (urls != undefined  && urls != 'undefined'){
    $('#options-URLsTextArea').val(`${tabs.atoURLsJSON}`);
  }
}

fillTabs();

// --------------------------
// Import URLs from text area
// --------------------------
$('.button-options-save').click(function(){
	var urls = $('#options-URLsTextArea').val();

  if (urls == ''){
    alert( browser.i18n.getMessage('emptyBackup') );
    // The text area must not contain any invalid content,
    // so we're re-filling it with the actual content:
    fillTabs();
    return;
  }

  // We're currently not checking whether the user-entered content is valid.
  atoURLs = JSON.parse(`${urls}`);

  var atoURLsJSON = JSON.stringify(atoURLs);
	browser.storage.local.set({ atoURLsJSON });

  alert(browser.i18n.getMessage('backupSuccess'));
  fillTabs();
});

// ---------------
// Delete all URLs
// ---------------
$('.button-options-delete').click(function(){
	browser.storage.local.remove('atoURLsJSON');
  alert(browser.i18n.getMessage('backupDeleteSuccess'));
  fillTabs();
});