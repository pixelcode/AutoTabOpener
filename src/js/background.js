window.browser = window.browser || window.chrome;

// ---------------------------------------
// Create button in context menu for links
// ---------------------------------------
browser.contextMenus.create({
    id: "add-url-to-ato",
    title: browser.i18n.getMessage('addToATO'),
    contexts: ["link"],
});

// -----------------------------------------
// User clicks on ATO button in context menu
// -----------------------------------------
browser.contextMenus.onClicked.addListener((info, tab) => {
  if (info.menuItemId === "add-url-to-ato") {
    const safeURL = escapeHTML(info.linkUrl);

    if (protocolCheck(safeURL)){
      // Load existing atoURLs
      browser.storage.local.get('atoURLsJSON').then(function(tabs) {
        addNewURL(tabs,safeURL);
      }).catch((error) => {
        console.error(error);
        // We need to pass undefined since we haven't got a tabs object:
        addNewURL(undefined,safeURL);
      });
    } else {
      console.error("Error: Can't add \"" + safeURL + "\" to Auto Tab Opener since it's not a regular URL.");
    }
  }
});

function addNewURL(tabs,url){
  var atoURLs = { pinned: [], regular: [] };

  if (tabs != undefined && tabs != 'undefined'){
    atoURLs = JSON.parse(`${tabs.atoURLsJSON}`);
  }

  atoURLs.regular.push(url);
  atoURLsJSON = JSON.stringify(atoURLs);
	browser.storage.local.set({ atoURLsJSON });
}

function protocolCheck(url) {
	regexp = /^((https?:\/\/)|(ftp:\/\/))/; // protocol must be either http:// or ftp://
	return regexp.test(url);
}

// https://gist.github.com/Rob--W/ec23b9d6db9e56b7e4563f1544e0d546
function escapeHTML(str) {
  // Note: string cast using String; may throw if `str` is non-serializable, e.g. a Symbol.
  // Most often this is not the case though.
  return String(str)
    .replace(/&/g, "&amp;")
    .replace(/"/g, "&quot;").replace(/'/g, "&#39;")
    .replace(/</g, "&lt;").replace(/>/g, "&gt;");
}