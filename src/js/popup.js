window.browser = window.browser || window.chrome;

// -------------------------------------------------
// Can the current website be added to the URL list?
// -------------------------------------------------
function checkAddability(){
	browser.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
		var currentURL = tabs[0].url;

		if (!(protocolCheck(currentURL))) {
			$('.addCurrent').addClass('addCurrent--unsupported');
			var addCurrentError = browser.i18n.getMessage('addCurrentError');
			$('#addCurrentButton').text(addCurrentError);
		}
	});
}

// Check addability when popup is opened
$(document).ready(function(){
	checkAddability();
});

// Since getting the stored URLs is asynchronous, it's more convenient to just
// declare a global variable that will be updated each time the stored
// URLs are updated too.
var atoURLs = new Object(),
	pinned = [],
	regular = [];

// ----------------
// Load stored URLs
// ----------------
browser.storage.local.get('atoURLsJSON').then(function(tabs) {
	return gotTabs(tabs);
}).catch((error) => {
  console.error(error);
});

function gotTabs(tabs){
	atoURLs = JSON.parse(`${tabs.atoURLsJSON}`);

	// insertNewURL() inserts elements at the top.
	// That's why we're looping through the URLs from the bottom,
	// starting with the regular tabs.
	for (var i = atoURLs.regular.length - 1; i >= 0; i--) {
		insertNewURL(atoURLs.regular[i],false);
	}

	for (var i = atoURLs.pinned.length - 1; i >= 0; i--) {
		insertNewURL(atoURLs.pinned[i],true);
	}
}

// ---------------------------
// Add current URL to the list
// ---------------------------
$('.addCurrent').click(function(){
	browser.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
		var url = tabs[0].url;
		var pinned = tabs[0].pinned;

		if(protocolCheck(url)){
			// remove http:// and ftp:// from URL because it would look ugly
			url = url.replace(/^(https?|ftps?):\/\//, '');
			insertNewURL(url,pinned);
			saveURLs();
		}
	});
});

// -------------
// Go to Mozilla
// -------------
$('.header-text').click(function(){
	browser.tabs.create({
		url: 'https://addons.mozilla.org/firefox/addon/auto-tab-opener'
	});
});

// ------------------------------------
// User toggles pin icon for single URL
// ------------------------------------
// Note: This specific syntax for OnClick events MUST be used
// for dynamically created elements! Right, dumbass!
$(document).on('click', '.url-pin-button', function(){
	var url = $(this).next('.url-input').val();

	// When the user clicks on the pin button, we do not know whether the current
	// value of the URL input field is valid, since it can be edited at any time.
	if (validateURL(url)){
	  $(this).toggleClass('url-pin-button--active');
		$(this).parent().next('.url-warning').removeClass('url-warning-visible');
		saveURLs();
	} else {
		$(this).parent().next('.url-warning').addClass('url-warning-visible');
	}
});

// ------------------------------------------------
// Save changes to URL input field or display error
// ------------------------------------------------
// Since the URL input fields can be edited at any time and there is no
// save button for them, we need to update the URL list after any interaction
// with any URL input field.
function urlInputChange(element){
	var url = $(element).val();

	if (validateURL(url)) {
		saveURLs();
		$(element).parent().next('.url-warning').removeClass('url-warning-visible');
	}

	// Only warn about invalid URL if input field is not empty
	if ( (!(validateURL(url))) && url != '') {
		$(element).parent().next('.url-warning').addClass('url-warning-visible');
	}
}

// ----------------------------------------------------------------
// User clicks in or out of a URL input field or hits any key there
// ----------------------------------------------------------------
$(document).on('keyup focusin focusout', '.url-input-item', function(){
	urlInputChange(this);
});

// ------------------
// Try to add new URL
// ------------------
function addNewURL(url,element,kind){
	// Obviously, we must validate the URL first.
	if (validateURL(url)) {
		insertNewURL(url,false);

		if (kind == 'inputField'){
			$(element).val('');
		}

		if (kind == 'button'){
			$(element).prev('.url-input').val('');
		}

		$(element).parent().next('.url-warning').removeClass('url-warning-visible');
		saveURLs();
	} else {
		$(element).parent().next('.url-warning').addClass('url-warning-visible');
	}
}

// ----------------------------
// Hit enter key to add new URL
// ----------------------------
$('.url-input-main').keypress(function(event){
  var keycode = (event.keyCode ? event.keyCode : event.which);

  if (keycode == '13'){ // enter key = #13
		var url = $(this).val();
		addNewURL(url,this,'inputField');
  }
});

// --------------------------------
// Click plus button to add new URL
// --------------------------------
$('.url-button-plus').click(function(){
  var url = $(this).prev('.url-input').val();
	addNewURL(url,this,'button');
});

// --------------------
// Remove URL from list
// --------------------
$(document).on('click', '.url-button-minus', function(){
	$(this).parent().parent().remove();
	saveURLs();
});

// -------------------
// Reset complete list
// -------------------
$('.button-delete').click(function(){
	$('.url-single').not('.url-single:first-child').remove();
	$('.url-warning').removeClass('url-warning-visible');

	browser.storage.local.remove('atoURLsJSON');

	atoURLs.pinned = [];
	atoURLs.regular = [];
});

// -------------
// Open all tabs
// -------------
$('.button-open').click(function(){

	// First, open all tabs to be pinned:
	atoURLs.pinned.forEach(function(url){
		if(protocolCheck(url) == false){
			url = 'https://' + url;
		}

		browser.tabs.create({
			url: url,
			pinned: true
		});
	});

	// Then, open all regular tabs:
	atoURLs.regular.forEach(function(url){
		if(protocolCheck(url) == false){
			url = 'https://' + url;
		}

		browser.tabs.create({
			url: url
		});
	});
});

// ---------------------------------------------
// check whether a URL could be opened correctly
// ---------------------------------------------
function validateURL(url) {
  var pattern = new RegExp('((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))'+ 			// OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ 	// port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?'+ 				// query string
    '(\\#[-a-z\\d_]*)?$','i'); 					// fragment locator
	return !!pattern.test(url);
}

function protocolCheck(url) {
	regexp = /^((https?:\/\/)|(ftp:\/\/))/; // protocol must be either http:// or ftp://
	return regexp.test(url);
}

// --------------------------------
// create new URL item in the popup
// --------------------------------
function insertNewURL(url,pinned) {
	var pinnedCode = '';

	if (pinned){
		pinnedCode = 'url-pin-button--active';
	}

	// insert HTML for new input field
  $('.url-single:first-child').after('<div class="url-single"><span class="url-single-top"><i class="fa-solid fa-thumbtack url-pin-button ' + pinnedCode + '" title="Pin tab" i18n-title="pinTab"></i><input value="' + url + '" placeholder="example.com" class="url-input url-input-item" type="text"></input><i class="fas fa-minus-circle url-button url-button-minus" title="Remove website" i18n-title="minusTitle"></i></span><span class="url-warning" i18n-content="invalidURL">Invalid URL!</span></div>');

	localiseAll();
	// Since localising the popup overwrites the text of the "add current website" button,
	// we need to re-check the addability.
	checkAddability();
}

// ---------------------------
// store the complete URL list
// ---------------------------
function saveURLs(){
	var pinnedTabs = [];
	var regularTabs = [];

	$('.url-single').each(function(){
		var url = $(this).find('.url-input-item').val();

		if(validateURL(url)){
			// Shall the tab be pinned?
			if ($(this).find('.url-pin-button').hasClass('url-pin-button--active')){
				pinnedTabs.push(url);
			} else {
				regularTabs.push(url);
			}
		}
	});

	atoURLs.pinned = pinnedTabs;
	atoURLs.regular = regularTabs;

	atoURLsJSON = JSON.stringify(atoURLs);
	browser.storage.local.set({ atoURLsJSON });
}