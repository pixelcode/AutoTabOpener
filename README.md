# Auto Tab Opener
**Auto Tab Opener** is a simple Firefox addon that allows you to open multiple URLs at once and to store them locally.

[![Get the Add-on](https://codeberg.org/pixelcode/AutoTabOpener/raw/commit/052a1ab8cd35ef6d83137d5c0ff19231147f2be2/get-the-addon.png)](https://addons.mozilla.org/firefox/addon/auto-tab-opener?utm_source=codeberg&utm_medium=readme&utm_content=get-the-addon-button)

## Purpose
Use **Auto Tab Opener** for example if you have several social media pages open all the time and don't want to open them manually every time you restart Firefox or your computer. After the restart you only have to click "open" because **Auto Tab Opener** saves all your URLs locally right after you enter them.

## Screenshots

| Popup | Options page | Right-clicking on link |
| ----- | ------------ | ---------------------- |
| ![Screenshot popup](https://codeberg.org/pixelcode/AutoTabOpener/raw/commit/8993e6eb9e985ddfdcf0294b219844678798165c/assets/v2.1%20popup.png) | ![Screenshot options](https://codeberg.org/pixelcode/AutoTabOpener/raw/commit/8993e6eb9e985ddfdcf0294b219844678798165c/assets/v2.1%20options.png) | ![Screenshot context menu](https://codeberg.org/pixelcode/AutoTabOpener/raw/commit/63986e57bf36b134544871c7611187d897c249f3/assets/v2.6%20context%20menu.png) |

### Languages

[![Translation status](https://translate.codeberg.org/widgets/auto-tab-opener/-/287x66-black.png)](https://translate.codeberg.org/engage/auto-tab-opener/)

[![Translation status](https://translate.codeberg.org/widgets/auto-tab-opener/-/multi-auto.svg)](https://translate.codeberg.org/engage/auto-tab-opener/)

Thanks to @mondstern (translator-in-chief) for his many translations! ❤️

Do you speak another language fluently? Then consider translating **Auto Tab Opener** via [Weblate](https://translate.codeberg.org/engage/auto-tab-opener)!

### How to add new websites
- In the addon’s popup: Enter URL and click on ➕ icon
- In the addon’s popup: Click on `Add current website` button
- On any website: Right-click on link and select `Add to Auto Tab Opener`

### Supported URLs

| Example | Explanation |
| ------- | ----------- |
| `example.com` | leaving out the protocol is okay |
| `http://example.com` | adding the protocol is also okay |
| `https://example.com` | both HTTP and HTTPS are okay |
| `www.example.com` | sub-domains are okay too |

### URLs not supported

| Example | Explanation |
| ------- | ----------- |
| `example` | missing top-level domain |
| `about:about` | internal Firefox pages |
| `file://example.html` | local files |

### Remove URLs
To remove a URL, simply click the corresponding minus icon. You can delete all websites by clicking on the “Reset” in the options under about:addons.

### How to pin the tabs
If you want to pin a tab, click the pin icon in front of the respective URL's input field.

### How to do a backup
If you want to export your URLs to a backup file go to **Auto Tab Opener**'s about:addons page and click on "Settings". There you can export and restore your URLs.

### How to report a bug
Simply open a [new issue](https://codeberg.org/pixelcode/AutoTabOpener/issues) and explain what happened (you need a Codeberg account).

### Rate Auto Tab Opener
Also, you can write a review on [Mozilla](https://addons.mozilla.org/firefox/addon/auto-tab-opener/reviews?utm_source=codeberg&utm_medium=readme&utm_content=review-link) (you need a Firefox account).

## Licence

You may use **Auto Tab Opener's** source code according to the [For Good Eyes Only Licence v0.2](https://codeberg.org/pixelcode/AutoTabOpener/src/branch/master/For%20Good%20Eyes%20Only%20Licence%20v0.2.pdf). Don't re-use its name, logos, icons or banners.

All versions of Auto Tab Opener are licensed under v0.2, which replaces v0.1 under which Auto Tab Opener was previously licensed. v0.1 is therefore void for any versions of Auto Tab Opener.

![](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/raw/branch/master/banner/Banner%20250.png)

-----

**jQuery:** Auto Tab Opener uses jQuery according to the [MIT Licence](https://github.com/jquery/jquery/blob/main/LICENSE.txt). &copy; Copyright OpenJS Foundation and other contributors, https://openjsf.org/

**Font Awesome:** Auto Tab Opener uses Font Awesome icons according to the [Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0) license.
